import { Component, OnInit, ViewChild } from '@angular/core';
import { ItemsdbService } from '../itemsdb.service';
import { ShoppingItem} from '../../shoppingitem';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  itemCount: number;
  items = Array<ShoppingItem>();
  newItem: string;
  newItemQuantity: any;
  @ViewChild('myInput', null) myInput; // ViewChild is used to reference to a HTML element from code.

  constructor(private itemsDB: ItemsdbService) { }

  ngOnInit() {
    // Get data asynchronously.
    this.itemsDB.getAll()
      .then(data => {
        // Read data by converting to String and parsing JSON.
        this.items = JSON.parse(String(data));
        if (!this.items) {
          // If there is nothing in storage, create empty array and UI is updated correctly.
          this.items = [];
        }
        this.itemCount = this.items.length;
      }, err => {
        // Error is print out to console for debugging purposes. In real app you should
        // notify user that something has gone wrong.
        console.log(err);
      });
  }

  /**
   * Adds new item to localstorage.
   * 
   * @param event Event parameter from input field, which can be used, for example, to determine
   *              which key was pressed.
   */
  addItem(event) {
    // If enter is pressed on input field, add item.
    if (event.keyCode === 13) {
      // Create new shopping item and save it to local store.
      const newShoppingItem: ShoppingItem = new ShoppingItem();
      newShoppingItem.name = this.newItem;
      newShoppingItem.quantity = this.newItemQuantity;
      this.items.push(newShoppingItem);
      // Reset UI for next new item.
      this.newItem = '';
      this.newItemQuantity = 1;
      this.myInput.setFocus(); // Set focus to input field (by using ViewChild).
      this.itemCount = this.items.length;
      this.itemsDB.save(this.items);
    }
  }

  /**
   * Removes item from localstorage.
   * 
   * @param index Index defines item to be removed.
   */
  removeItem(index) {
    this.items.splice(index,1);
    this.itemsDB.save(this.items);
  }

}
